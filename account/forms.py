from django import forms
from django.contrib.auth.forms import AuthenticationForm

from .models import UserBasic


class RegistryForm(forms.ModelForm):
    user_name = forms.CharField(label='Enter Username',
                                min_length=5,
                                max_length=48,
                                help_text='Required')
    email = forms.EmailField(max_length=128,
                             help_text='Required',
                             error_messages={
                                 'required': 'Sorry, you will need an email'
                             }
                             )
    password = forms.CharField(label='Password',
                               widget=forms.PasswordInput)
    verify_password = forms.CharField(label='Repeat password',
                                      widget=forms.PasswordInput)

    class Meta:
        model = UserBasic
        fields = ('user_name', 'email')

    def check_username(self):
        user_name = self.cleaned_data['user_name'].lower()
        result_check = UserBasic.objects.filter(user_name=user_name)
        if result_check.count():
            raise forms.ValidationError("Username already exists")
        return user_name

    def check_password(self):
        cd = self.cleaned_data
        if cd['password'] != cd['verify_password']:
            raise forms.ValidationError('Passwords do not match.')
        return cd['verify_password']

    def check_email(self):
        email = self.cleaned_data['email']
        if UserBasic.objects.filter(email=email).exists():
            raise forms.ValidationError(
                'Email already exists')
        return email

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['user_name'].widget.attrs.update(
            {'class': 'form-control mb-3', 'placeholder': 'Username'})
        self.fields['email'].widget.attrs.update(
            {'class': 'form-control mb-3', 'placeholder': 'E-mail', 'name': 'email', 'id': 'id_email'})
        self.fields['password'].widget.attrs.update(
            {'class': 'form-control mb-3', 'placeholder': 'Password'})
        self.fields['verify_password'].widget.attrs.update(
            {'class': 'form-control', 'placeholder': 'Repeat Password'})


class UserLoginForm(AuthenticationForm):
    username = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control mb-3', 'placeholder': 'Username', 'id': 'login-username'}))
    password = forms.CharField(widget=forms.PasswordInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Password',
            'id': 'login-pwd',
        }
    ))


class UserEditForm(forms.ModelForm):
    email = forms.EmailField(
        label='Account email (cannot be changed)', max_length=128, widget=forms.TextInput(
            attrs={'class': 'form-control mb-3', 'placeholder': 'email', 'id': 'form-email', 'readonly': 'readonly'}))

    user_name = forms.CharField(
        label='Firstname', min_length=4, max_length=48, widget=forms.TextInput(
            attrs={'class': 'form-control mb-3', 'placeholder': 'Username', 'id': 'form-firstname',
                   'readonly': 'readonly'}))

    first_name = forms.CharField(
        label='Firstname', min_length=4, max_length=48, widget=forms.TextInput(
            attrs={'class': 'form-control mb-3', 'placeholder': 'Firstname', 'id': 'form-lastname'}))

    class Meta:
        model = UserBasic
        fields = ('email', 'user_name', 'first_name',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['user_name'].required = True
        self.fields['email'].required = True
