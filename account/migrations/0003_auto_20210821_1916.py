# Generated by Django 3.2.6 on 2021-08-21 17:16

from django.db import migrations, models
import django_countries.fields


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0002_alter_userbasic_email'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userbasic',
            name='address',
            field=models.CharField(blank=True, max_length=128),
        ),
        migrations.AlterField(
            model_name='userbasic',
            name='city',
            field=models.CharField(blank=True, max_length=128),
        ),
        migrations.AlterField(
            model_name='userbasic',
            name='country',
            field=django_countries.fields.CountryField(blank=True, max_length=2),
        ),
        migrations.AlterField(
            model_name='userbasic',
            name='email',
            field=models.EmailField(max_length=128, unique=True, verbose_name='email address'),
        ),
        migrations.AlterField(
            model_name='userbasic',
            name='first_name',
            field=models.CharField(blank=True, max_length=128),
        ),
        migrations.AlterField(
            model_name='userbasic',
            name='last_name',
            field=models.CharField(blank=True, max_length=128),
        ),
        migrations.AlterField(
            model_name='userbasic',
            name='phone_number',
            field=models.CharField(blank=True, max_length=13),
        ),
        migrations.AlterField(
            model_name='userbasic',
            name='postcode',
            field=models.CharField(blank=True, max_length=10),
        ),
        migrations.AlterField(
            model_name='userbasic',
            name='user_name',
            field=models.CharField(max_length=48, unique=True),
        ),
    ]
