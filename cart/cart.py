from catalogue.models import Product
from decimal import Decimal


class Cart:
    def __init__(self, request):
        self.session = request.session
        cart = self.session.get('session_key')
        if 'session_key' not in request.session:
            cart = self.session['session_key'] = {}
        self.cart = cart

    def add(self, product, qty):
        """
        Adding items to cart
        """
        product_id = str(product.id)

        if product_id in self.cart:
            self.cart[product_id]['qty'] += qty
        else:
            self.cart[product_id] = {'price': str(product.price), 'qty': qty}

        self.save()

    def __iter__(self):
        """
        Get products ids and returning their prices
        """

        product_ids = self.cart.keys()
        products = Product.objects.filter(id__in=product_ids)
        cart = self.cart.copy()

        for product in products:
            cart[str(product.id)]['product'] = product

        for item in cart.values():
            item['price'] = Decimal(item['price'])
            item['total_price'] = item['price'] * item['qty']
            yield item

    def get_subtotal_price(self):
        """
        :return: Total sum of all items in shopping cart
        """

        return sum(Decimal(item['price']) * item['qty'] for item in self.cart.values())

    def get_total_price(self):
        """
        :return: Total sum of all items plus shipping
        """
        subtotal = sum(Decimal(item['price']) * item['qty'] for item in self.cart.values())

        if subtotal == 0:
            shipping = Decimal(0.00)
        else:
            shipping = Decimal(11.50)

        total = subtotal + Decimal(shipping)
        return total

    def __len__(self):
        """
        :return: Total sum of items in cart
        """
        return sum(item['qty'] for item in self.cart.values())

    def delete(self, product):
        """
        Delete products from cart
        """
        product_id = str(product)

        if product_id in self.cart:
            del self.cart[product_id]
            self.save()

    def update(self, product, qty):
        """
        Update quantity of products in cart
        """
        product_id = str(product)

        if product_id in self.cart:
            self.cart[product_id]['qty'] = qty
        self.save()

    def save(self):
        self.session.modified = True
