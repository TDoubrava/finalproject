var stripe = Stripe('pk_test_51JQysIAwM9YreJLak5dET0GHs5ivVAJLShW3aqFPV6VCRT7gWwnPJ7EfqBfLeOU2ePF85aEnDO1rdypbhP9n7RUT007wF7cDop');

var elem = document.getElementById('submit');
clientsecret = elem.getAttribute('data-secret');

var elements = stripe.elements();
var style = {
base: {
  color: "#000",
  lineHeight: '2.4',
  fontSize: '16px'
    }
};

var card = elements.create("card", { style: style });
card.mount("#card-element");

card.on('change', function(event) {
var displayError = document.getElementById('card-errors')

if (event.error) {
  displayError.textContent = event.error.message;
  $('#card-errors').addClass('alert alert-info');
} else {
  displayError.textContent = '';
  $('#card-errors').removeClass('alert alert-info');
}
});
