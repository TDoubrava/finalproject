from django.contrib.auth.decorators import login_required
from django.shortcuts import render
import stripe

from cart.cart import Cart


@login_required
def CartView(request):
    cart = Cart(request)
    total = str(cart.get_total_price())
    total = total.replace('.', '')
    total = int(total)

    stripe.api_key = 'sk_test_51JQysIAwM9YreJLawEFh85LiUQJlRERdi1bzEEKXyvSxgd2tAIkxiCg4aceXfUhcY6Zj5lGKzAIFhfTmBRl1UEyx00iminW1Nf'
    intent = stripe.PaymentIntent.create(
        amount=total,
        currency='usd',
        metadata={'userid': request.user.id}
    )

    return render(request, 'payment/home.html', {'client_secret': intent.client_secret})


