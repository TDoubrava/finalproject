from django.db import models
from django.urls import reverse


class Category(models.Model):
    name = models.CharField(max_length=126)
    slug = models.SlugField(max_length=126, unique=True)

    class Meta:
        verbose_name_plural = 'categories'
        ordering = ('name',)

    def get_absolute_url(self):
        return reverse('catalogue:category_list', args=[self.slug])

    def __str__(self):
        return self.name


class Producer(models.Model):
    name = models.CharField(max_length=126)
    slug = models.SlugField(max_length=126, unique=True)

    # TODO logo = models.ImageField(upload_to='uploads/')

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name


class Product(models.Model):
    category = models.ForeignKey(Category, related_name='products', on_delete=models.PROTECT)
    producer = models.ForeignKey(Producer, related_name='producers', on_delete=models.PROTECT)
    slug = models.SlugField(max_length=128, unique=True)
    name = models.CharField(max_length=126)
    description = models.TextField(blank=True, null=True)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    in_stock = models.BooleanField(default=True)
    is_active = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(upload_to='images/', default='images/default.png')

    # TODO: thumbnail = models.ImageField(upload_to='uploads/', blank=True, null=True)

    class Meta:
        ordering = ('-created',)

    def get_absolute_url(self):
        return reverse('catalogue:product_detail', args=[self.slug])

    def __str__(self):
        return self.name
